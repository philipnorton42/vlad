Vlad - Vagrant LAMP Ansible Drupal
==================================

A Drupal development platform in a box, with everything you would need to develop Drupal websites.

**We have moved!**

This repository is an empty placeholder and has no issue tracker or source code.

Checkout our new home at GitHub here https://github.com/hashbangcode/vlad